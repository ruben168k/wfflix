<div class="container text-center text-md-start">
    <div class="row">
        <?php foreach ($videos as $video) : ?>
            <div class="col-xs-12 col-md-6 col-lg-4 col-xl-3 mb-3">
                <a href="/videos/watch?<?= $video['id'] ?>">
                    <div class="position-relative">
                        <img class="img-fluid"
                             src="http://img.youtube.com/vi/<?= $video['youtube_link'] ?>/mqdefault.jpg"
                             alt="Thumbnail">
                        <img class="position-absolute top-50 start-50 translate-middle"
                             style="width: 50px; opacity: 0.9" src="../img/play.png" alt="start">
                    </div>
                </a>
                <h6 class="mt-2"><?= $video['title'] ?></h6>
            </div>
        <?php endforeach; ?>
    </div>
</div>