<div class="container">
    <a class="btn btn-success text-white mb-4" href="/dashboard/videos/add">Video toevoegen</a>
    <div class="table-responsive">
        <table class="table">
            <thead>
            <tr>
                <th scope="col">Id</th>
                <th scope="col">Title</th>
                <th scope="col">Link</th>
                <th scope="col">Bewerk</th>
                <th scope="col">Delete</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($videos as $video) : ?>
                <tr>
                    <th scope="row"><?= $video['id'] ?></th>
                    <td><?= $video['title'] ?></td>
                    <td><a href="<?= $video['youtube_link'] ?>"><?= $video['youtube_link'] ?></a></td>
                    <td>
                        <a class="btn btn-primary text-white"
                           href="/dashboard/videos/edit?id=<?= $video['id'] ?>">Edit</a>
                    </td>
                    <td>
                        <form action="/dashboard/videos/delete?id=<?= $video['id'] ?>" method="post">
                            <button type="submit" class="btn btn-danger text-white">Delete</button>
                        </form>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
