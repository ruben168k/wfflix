<div class="container">
    <a class="btn btn-secondary text-white mb-4" href="/dashboard/videos">Terug</a>
    <h1>Video toevoegen</h1>
    <form action="" method="post">
        <div class="form-group mb-4">
            <label for="title">Title</label>
            <input type="text" name="title" class="form-control" id="title">
        </div>
        <div class="form-group">
            <label for="link">Youtube embedded code (bv. rzSPVsS2a-A)</label>
            <input type="text" name="link" class="form-control" id="link">
        </div>
        <button type="submit" class="btn btn-primary mt-4">Upload</button>
    </form>
</div>
