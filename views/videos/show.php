<div class="container">
    <a class="btn btn-secondary" href="/">Terug</a>
</div>
<div class="container my-5">
    <div class="row">
        <div class="col-lg-8 col-xl-7">
            <div class="ratio ratio ratio-16x9">
                <iframe class="embed-responsive-item"
                        src="https://www.youtube.com/embed/<?= $video['youtube_link'] ?>?rel=0"
                        allowfullscreen></iframe>
            </div>
        </div>
        <div class="col-lg-4 col-xl-5 mt-5 mt-lg-0">
            <h1><?= $video['title'] ?></h1>
        </div>
    </div>
</div>


