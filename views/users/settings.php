<div class="container">
    <h1>Account settings</h1>
    <div>
        <form action="" method="post">
            <div class="form-group mb-2">
                <label for="username">Username</label>
                <input type="text" name="username" class="form-control" id="username" value="<?= $user['name'] ?>">
            </div>
            <div class="form-group mb-2">
                <label for="email">Email</label>
                <input type="email" name="email" class="form-control" id="email" value="<?= $user['email'] ?>">
            </div>
            <input hidden name="id" value="<?= $user['id'] ?>">
            <button id="submit" class="btn btn-success mt-3 text-white" type="submit">Wijzigingen opslaan</button>
        </form>
        <form class="mt-2" action="/logout" method="post">
            <button class="btn btn-danger" type="submit">Logout</button>
        </form>
    </div>
</div>