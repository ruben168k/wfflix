<div class="bcol-12 col-sm-10 col-md-8 col-lg-6">
    <?php if (!empty($errors)) : ?>
        <div class="bg-danger p-3 mb-2 border-1 border-red rounded">
            <?php foreach ($errors as $error) : ?>
                <p class="text-white">* <?= $error ?></p>
            <?php endforeach; ?>
        </div>
    <?php endif; ?>
    <form class="bg-white p-5 shadow rounded" action="" method="post">
        <div class="row">
            <div class="form-group my-3 col-xl-6">
                <label for="username">Username</label>
                <input type="text" name="username" class="form-control" id="username">
            </div>
            <div class="form-group my-3 col-xl-6">
                <label for="email">Email</label>
                <input type="email" name="email" class="form-control" id="email">
            </div>
        </div>
        <div class="form-group my-3">
            <label for="password">Password</label>
            <input type="password" name="password" class="form-control" id="password">
        </div>
        <div class="form-check my-4">
            <input onclick="showInput()" class="form-check-input" type="checkbox" value="" id="teacher">
            <label class="form-check-label" for="teacher">
                Sign up as teacher?
            </label>
        </div>
        <div class="form-group" id="act_form-group" hidden>
            <label for="act_code">Activatie code</label>
            <input type="text" name="act_code" class="form-control" id="act_code">
        </div>
        <button type="submit" class="btn btn-primary text-white mt-4">Signup</button>
    </form>
</div>
<script>
    const checkbox = document.getElementById('teacher');
    const input = document.getElementById('act_form-group')

    function showInput() {
        if (checkbox.checked) {
            input.toggleAttribute("hidden")
        } else {
            input.toggleAttribute("hidden")
        }
    }
</script>
