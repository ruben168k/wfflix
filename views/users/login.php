<div class="col-12 col-sm-10 col-md-8 col-lg-4">
    <?php if (!empty($errors)) : ?>
        <div class="bg-danger p-3 mb-2 border-1 border-red rounded">
            <?php foreach ($errors as $error) : ?>
                <p class="text-white">* <?= $error ?></p>
            <?php endforeach; ?>
        </div>
    <?php endif; ?>
    <form class="bg-white p-5 shadow rounded" action="" method="post">
        <div class="form-group my-3">
            <label for="username">Username</label>
            <input type="text" name="username" class="form-control" id="username"
                   value="<?php if (isset($_SESSION['user']['name'])) {
                       echo $_SESSION['user']['name'];
                   } ?>">
        </div>
        <div class="form-group my-3">
            <label for="password">Password</label>
            <input type="password" name="password" class="form-control" id="password">
        </div>
        <button type="submit" class="btn btn-primary text-white mt-4">Signup</button>
    </form>
</div>
