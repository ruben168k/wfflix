<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link href="../css/app.css" rel="stylesheet">

    <title>Hello, world!</title>
</head>
<body class="bg-warning">
<div class="container vh-100 d-flex align-items-center justify-content-center" style="min-height: calc(100vh - 160px)">
    {{content}}
</div>
<footer class="w-100 p-3 bg-light text-center border-warning border-top">
    <p class="m-0">Alle rechten voorbehouden aan WFFLIX</p>
</footer>
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-OERcA2EqjJCMA+/3y+gxIOqMEjwtxJY7qPCqsdltbNJuaOe923+mo//f6V8Qbsw3"
        crossorigin="anonymous"></script>
</body>
</html>
