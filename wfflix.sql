use wfflix;

create table if not exists users
(
    id             int primary key not null auto_increment,
    name           varchar(32)     not null,
    email          varchar(64),
    password       varchar(64),
    roles_id       int,
    email_verified int,
    act_code       varchar(32),
    cr_at          datetime,
    up_at          datetime,
    del_at         datetime
);

create table if not exists videos
(
    id           int primary key not null auto_increment,
    title        varchar(32)     not null,
    youtube_link varchar(512)    not null,
    time         int             not null,
    courses_id   int,
    cr_at        datetime,
    up_at        datetime,
    del_at       datetime
);

/*  start inhoud van tables */

insert into users (name, email, password, roles_id, email_verified, act_code, cr_at, up_at, del_at)
values ('antonaso', 'anton-meun@hotmail.com', '123456789', '2', 7685, '1928', date(now()), date(now()), NULL);

insert into users (name, email, password, roles_id, email_verified, act_code, cr_at, up_at, del_at)
values ('henk', 'anton-meun@hotmail.com', '123456789', '2', 7685, '4567', date(now()), date(now()), NULL);

insert into videos (title, youtube_link, time, courses_id, cr_at, up_at, del_at)
values ('php termen', 'U10yvfIStx8', 432, '1', date(now()), date(now()), NULL);


/*  end inhoud van tables */

/* start activering */

select *
from users;

select *
from roles;

select *
from videos;

select *
from courses;

# SOFT DELETE USER
update users
set del_at = null;

update users
set del_at = date(now()),
    up_at  = date(now())
where id = 1;

/* end activering */

/*start tabel weer weg halen*/

drop table users;
drop table roles;
drop table videos;
drop table courses;

/*end tabel weer weg halen*/

