<?php

namespace app\controllers;

use app\core\Controller;
use app\core\Request;
use app\models\LoginModel;
use app\models\UserModel;

class AuthController extends Controller
{

    public function login(Request $request)
    {
        if (isset($_SESSION['user']['loggedIn']) && $_SESSION['user']['loggedIn'] === true)
        {
            return $this->redirect('/');
        }

        $loginModel = new LoginModel();

        if ($request->isPost())
        {


            if (empty($loginModel->validate($_POST)) && $loginModel->login($_POST))
            {
                return $this->redirect('/');
            }
        }

        $this->setLayout('auth');
        return $this->render('users/login', []);
    }

    public function signUp(Request $request)
    {
        if (isset($_SESSION['user']['loggedIn']) && $_SESSION['user']['loggedIn'] === true)
        {
            return $this->redirect('/');
        }

        $userModel = new UserModel();

        if ($request->isPost())
        {

            if (empty($userModel->validate($_POST)) && $userModel->create($_POST))
            {

                return $this->redirect('/login');
            }

            if (array_count_values($userModel->validate($_POST)) > 0)
            {
                $this->setLayout('auth');
                return $this->render('users/signup', [
                    'model' => $userModel,
                    'errors' => $userModel->validate($_POST)
                ]);
            }

            return $this->render('users/login', [
                'model' => $userModel
            ]);
        }

        $this->setLayout('auth');
        return $this->render('users/signup', [
            'model' => $userModel
        ]);
    }

    public function logout(Request $request)
    {
        if (isset($_SESSION['user']['loggedIn']))
        {
            session_destroy();
        }

        return $this->redirect('/');
    }

}