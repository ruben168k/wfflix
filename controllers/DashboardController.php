<?php

namespace app\controllers;

use app\core\Controller;
use app\models\AuthModel;

class DashboardController extends Controller
{
    public function index()
    {
        $AuthModel = new AuthModel();
        if ($AuthModel->auth($_SESSION['user']['id']) !== 2)
        {
            return $this->redirect('/login');
        }

        if (isset($_SESSION['user']))
        {
            $user = $_SESSION['user'];
            $username = $user['name'];
        } else {
            $username = '';
        }

        $this->setLayout('dashboard');
        return $this->render('dashboard', [
            'name' => $username,
        ]);
    }
}