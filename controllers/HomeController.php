<?php

namespace app\controllers;

use app\core\Controller;
use app\models\VideoModel;

class HomeController extends Controller
{
    public function index()
    {
        if (isset($_SESSION['user']))
        {
            $user = $_SESSION['user'];
            $username = $user['name'];
        }
        else {
            $username = '';
        }

        $videoModel = new VideoModel();
        $videos = $videoModel->read('');


        return $this->render('home', [
            'username' => $username,
            'videos' => $videos
        ]);
    }
}