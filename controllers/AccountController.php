<?php

namespace app\controllers;

use app\core\Controller;
use app\core\Request;
use app\models\UserModel;

class AccountController extends Controller
{
    public function index(Request $request)
    {
        if (!isset($_SESSION['user']['loggedIn'])) {
            return $this->redirect('/login');
        }

        $userModel = new UserModel();
        $user = $userModel->read($_SESSION['user']['id']);

        return $this->render('users/settings', [
            'user' => $user
        ]);
    }

    public function edit(Request $request)
    {
        $userModel = new UserModel();

        if ($request->isPost())
        {

            if (empty($userModel->validateUpdate($_POST)) && $userModel->update($_POST))
            {

                $userModel = new UserModel();
                $user = $userModel->read($_SESSION['user']['id']);

                return $this->render('users/settings', [
                    'user' => $user,
                    'errors' => $userModel->validate($_POST)
                ]);
            }
        }

        $userModel = new UserModel();
        $user = $userModel->read($_SESSION['user']['id']);

        return $this->render('users/settings', [
            'user' => $user
        ]);
    }
}