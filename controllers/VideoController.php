<?php

namespace app\controllers;

use app\core\Controller;
use app\core\Request;
use app\models\AuthModel;
use app\models\VideoModel;

class VideoController extends Controller
{
    public function index()
    {
        $AuthModel = new AuthModel();
        if ($AuthModel->auth($_SESSION['user']['id']) != 2) {
            return $this->redirect('/login');
        }

        $videoModel = new VideoModel();
        $videos = $videoModel->read('');

        return $this->render('videos/index', [
            'videos' => $videos
        ]);
    }

    public function create(Request $request)
    {
        $AuthModel = new AuthModel();
        if ($AuthModel->auth($_SESSION['user']['id']) != 2) {
            return $this->redirect('/login');
        }

        $videoModel = new VideoModel();

        if ($request->isPost()) {
            if ($videoModel->validate($_POST) && $videoModel->create($_POST)) {
                return $this->redirect('/dashboard/videos');
            }
        }

        $this->setLayout('dashboard');
        return $this->render('videos/create', []);
    }

    public function edit(Request $request)
    {
        $AuthModel = new AuthModel();
        if ($AuthModel->auth($_SESSION['user']['id']) != 2) {
            return $this->redirect('/login');
        }

        $videoModel = new VideoModel();
        $id = explode('=', $_SERVER['QUERY_STRING']);
        $video = $videoModel->read($id[1]);

        if ($request->isPost()) {

            if ($videoModel->validate($_POST) && $videoModel->update($_POST, $id[1])) {

                return $this->redirect('/login');
            }

        }

        $this->setLayout('dashboard');
        return $this->render('videos/edit', [
            'video' => $video
        ]);
    }

    public function show()
    {
        $AuthModel = new AuthModel();
        if ($AuthModel->auth($_SESSION['user']['id']) < 1) {
            return $this->redirect('/login');
        }

        $videoModel = new VideoModel();
        $id = explode('=', $_SERVER['QUERY_STRING']);
        $video = $videoModel->read($id[0]);

        return $this->render('videos/show', [
            'video' => $video
        ]);
    }

    public function delete(Request $request)
    {

        $AuthModel = new AuthModel();
        if ($AuthModel->auth($_SESSION['user']['id']) != 2) {
            return $this->redirect('/login');
        }

        if ($request->isPost()) {

            $videoModel = new VideoModel();
            $id = explode('=', $_SERVER['QUERY_STRING']);
            $videoModel->delete($id[1]);

        }

        return $this->redirect('/dashboard/videos');
    }
}