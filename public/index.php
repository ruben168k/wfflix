<?php

require_once __DIR__ . '/../vendor/autoload.php';

use app\controllers\AccountController;
use app\controllers\AuthController;
use app\controllers\DashboardController;
use app\controllers\HomeController;
use app\controllers\VideoController;
use app\core\App;


$app = new App(dirname(__DIR__));

$app->router->get('/dashboard', [DashboardController::class, 'index']);
$app->router->get('/dashboard/videos', [VideoController::class, 'index']);
$app->router->get('/dashboard/videos/edit', [VideoController::class, 'edit']);
$app->router->post('/dashboard/videos/edit', [VideoController::class, 'edit']);
$app->router->get('/dashboard/videos/add', [VideoController::class, 'create']);
$app->router->post('/dashboard/videos/add', [VideoController::class, 'create']);
$app->router->get('/dashboard/videos/delete', [VideoController::class, 'delete']);
$app->router->post('/dashboard/videos/delete', [VideoController::class, 'delete']);


$app->router->get('/account/edit', [AccountController::class, 'index']);
$app->router->post('/account/edit', [AccountController::class, 'edit']);
$app->router->get('/videos/watch', [VideoController::class, 'show']);
$app->router->post('/logout', [AuthController::class, 'logout']);


$app->router->get('/', [HomeController::class, 'index']);
$app->router->get('/login', [AuthController::class, 'login']);
$app->router->post('/login', [AuthController::class, 'login']);
$app->router->get('/signup', [AuthController::class, 'signUp']);
$app->router->post('/signup', [AuthController::class, 'signUp']);

$app->run();
?>

