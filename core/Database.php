<?php

namespace app\core;
use PDO;

class Database
{
    public static function conn() {
        $config = parse_ini_file('../config.ini');

        $host = $config['db_host'];
        $dbname = $config['db_name'];
        $user = $config['db_user'];
        $pass = $config['db_password'];
        $charset = $config['db_charset'];

        $dsn = "mysql:host=$host;dbname=$dbname;charset=$charset";
        $options = [
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::ATTR_EMULATE_PREPARES => false,
        ];
        try {
            return new PDO($dsn, $user, $pass, $options);
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }
}