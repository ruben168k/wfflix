<?php

namespace app\core;

class App
{
    public static string $ROOT_DIR;
    public Router $router;
    public Request $request;
    public Response $response;
    public static App $app;
    public Controller $controller;
    public Database $database;

    public function __construct($rootPath)
    {
        self::$ROOT_DIR = $rootPath;
        self::$app = $this;
        $this->request = new Request();
        $this->response = new Response();
        $this->router = new Router($this->request, $this->response);
        $this->controller = new Controller();
        $this->database = new Database();
        $this->database->conn();
        session_start();
        if (!isset($_SESSION['user']))
        {
            $_SESSION['user'] = [
                'loggedIn' => false,
                'id' => null,
                'roles_id' => null,
                'name' => null,
                'email' => null,
            ];
        }

    }

    public function run()
    {
        echo $this->router->resolve();
    }

    /**
     * @return Controller
     */
    public function getController(): Controller
    {
        return $this->controller;
    }

    /**
     * @param Controller $controller
     */
    public function setController(Controller $controller): void
    {
        $this->controller = $controller;
    }

}