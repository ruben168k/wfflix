<?php

namespace app\models;

use app\core\Database;

class UserModel
{

    protected $conn;

    public function __construct()
    {
        $this->conn = Database::conn();
    }

    public function create($data)
    {
        $username = $data['username'];
        $password = $data['password'];
        $email = $data['email'];
        $act_code = $data['act_code'];

        if (empty($act_code)) {
            $sql = "INSERT INTO users (name, password, email, roles_id) VALUES(:name, :password, :email, :roles_id)";
            $stmt = $this->conn->prepare($sql, []);
            $stmt->execute(array(':name' => $username, ':password' => md5($password), ':email' => $email, ':roles_id' => 1));
        }
        else
        {
            $sql = "INSERT INTO users (name, password, email, roles_id, act_code) VALUES(:name, :password, :email, :roles_id, :act_code)";
            $stmt = $this->conn->prepare($sql, []);
            $stmt->execute(array(':name' => $username, ':password' => md5($password), ':email' => $email, ':roles_id' => 2, ':act_code' => $act_code));
        }


        $_SESSION['user'] = [
            'name' => $username
        ];

        return 'succes';
    }

    public function read($id)
    {
        if (empty($id))
        {
            $sql = "SELECT * FROM users";
            $stmt = $this->conn->prepare($sql);
            $stmt->execute();

            $users = $stmt->fetchAll();

            return $users;
        }
        else
        {
            $sql = "SELECT id, name, email, roles_id, email_verified, act_code FROM users WHERE id = :id";
            $stmt = $this->conn->prepare($sql, []);
            $stmt->execute(array(':id' => $id));

            $user = $stmt->fetch();

            return $user;
        }
    }

    public function update($data)
    {
        $id = $data['id'];
        $username = $data['username'];
        $email = $data['email'];

        $sql = "UPDATE users SET name = :username , email = :email WHERE id = :id";
        $stmt = $this->conn->prepare($sql, []);
        $stmt->execute(array(':username' => $username, ':email' => $email, ':id' => $id));

        header("Refresh:0");
    }

    public function delete()
    {

    }

    public function validate($data)
    {
        $errors = [];

        $required = [
            'username',
            'email',
            'password'
        ];

//        For each not filled required field add error.
        foreach($required as $field)
        {
            if (empty($data[$field]))
            {
                $field = ucfirst($field);
                array_push($errors, "$field is een verplicht veld.");
            }
        }

//        Check if username or email is taken.
        $sql = "SELECT name, email FROM users WHERE name = :username OR email = :email";
        $stmt = $this->conn->prepare($sql, []);
        $stmt->execute(array(':username' => $data['username'], ':email' => $data['email']));

        $count = $stmt->rowCount();

        if ($count != 0)
        {
            array_push($errors, 'Username of email is al eerder gebruikt.');
        }

        if (strlen($data['password']) < 8)
        {
            array_push($errors, 'Wachtwoord moet minstens 8 karakters bevatten.');
        }

        return $errors;
    }

//    Door tijd nood geen tijd om 1 enkele validatie functie te maken.

    public function validateUpdate($data)
    {
        $errors = [];

        $required = [
            'username',
            'email',
        ];

//        For each not filled required field add error.
        foreach($required as $field)
        {
            if (empty($data[$field]))
            {
                $field = ucfirst($field);
                array_push($errors, "$field is een verplicht veld.");
            }
        }
        
        return $errors;
    }

}