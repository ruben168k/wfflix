<?php

namespace app\models;

use app\core\Database;

class VideoModel
{

    protected $conn;

    public function __construct()
    {
        $this->conn = Database::conn();
    }

    public function create($data)
    {
        $title = $data['title'];
        $link = $data['link'];

        $sql = "INSERT INTO videos (title, youtube_link, time) VALUES(:title, :link, :time)";
        $stmt = $this->conn->prepare($sql, []);
        $stmt->execute(array(':title' => $title, ':link' => $link, ':time' => 20));

        return 'succes';
    }

    public function read($id)
    {
        if (empty($id))
        {
            $sql = "SELECT * FROM videos WHERE del_at IS NULL";
            $stmt = $this->conn->prepare($sql);
            $stmt->execute();

            $videos = $stmt->fetchAll();

            return $videos;
        }
        else
        {
            $sql = "SELECT * FROM videos WHERE id = :id AND del_at IS NULL";
            $stmt = $this->conn->prepare($sql, []);
            $stmt->execute(array(':id' => $id));

            $video = $stmt->fetch();

            return $video;
        }
    }

    public function update($data, $id)
    {
        $title = $data['title'];
        $link = $data['link'];

        $sql = "UPDATE videos SET title = :title , youtube_link = :link WHERE id = :id";
        $stmt = $this->conn->prepare($sql, []);
        $stmt->execute(array(':title' => $title, ':link' => $link, ':id' => $id));

        header("Refresh:0");
    }

    public function delete($id)
    {
        $sql = "UPDATE videos SET del_at = :date WHERE id = :id";
        $stmt = $this->conn->prepare($sql, []);
        $stmt->execute(array(':date' => date("Y-m-d H:i:s"), ':id' => $id ));

        return true;
    }

    public function validate()
    {
        return true;
    }

}