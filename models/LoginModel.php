<?php

namespace app\models;

use app\core\Database;

class LoginModel
{
    protected $conn;

    public function __construct()
    {
        $this->conn = Database::conn();
    }

    public function login($data)
    {
       $username = $data['username'];
       $password = md5($data['password']);

        $sql = "SELECT id, name, email, roles_id FROM users WHERE name = :username AND password = :password";
        $stmt = $this->conn->prepare($sql, []);
        $stmt->execute(array(':username' => $username, ':password' => $password));

        $user = $stmt->fetch();

       $_SESSION['user'] = [
           'loggedIn' => true,
           'id' => $user['id'],
           'roles_id' => $user['roles_id'],
           'name' => $user['name'],
           'email' => $user['email'],
       ];

       return true;
    }

    public function validate($data)
    {
        $errors = [];

        $required = [
            'username',
            'password'
        ];

//        For each not filled required field add error.
        foreach($required as $field)
        {
            if (empty($data[$field]))
            {
                $field = ucfirst($field);
                array_push($errors, "$field is een verplicht veld.");
            }
        }

//        Check if username and password is correct

        $sql = "SELECT name FROM users WHERE name = :username AND password = :password";
        $stmt = $this->conn->prepare($sql, []);
        $stmt->execute(array(':username' => $data['username'], ':password' => md5($data['password'])));

        if ($stmt->rowCount() === 0)
        {
            array_push($errors, "Combinatie gebruikersnaam / wachtwoord is incorrect..");
        }

        return $errors;
    }

}

