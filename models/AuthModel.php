<?php

namespace app\models;

use app\core\Database;

class AuthModel
{
    protected $conn;

    public function __construct()
    {
        $this->conn = Database::conn();
    }

    public function auth($id)
    {
        $sql = "SELECT roles_id FROM users WHERE id = :id";
        $stmt = $this->conn->prepare($sql, []);
        $stmt->execute(array(':id' => $id));
        $user = $stmt->fetch();

        if ($user) {
            return $user['roles_id'];
        }

        return 0;
    }

}